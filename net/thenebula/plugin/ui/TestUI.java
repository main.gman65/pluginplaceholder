package net.thenebula.plugin.ui;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import net.thenebula.plugin.utils.Utils;

public class TestUI {

	public static Inventory inv;
	public static String inventory_name;
	public static int inv_total = 4;
	public static int inv_rows = inv_total * 9;
	
	public static void initialize() {
		inventory_name = Utils.chat("&d&lThe Nebula &8| &5Test GUI");
		
		inv = Bukkit.createInventory(null, inv_rows);
	}
	
	public static Inventory GUI (Player p) {
			
		Inventory toReturn = Bukkit.createInventory(null, inv_rows, inventory_name);
	
		//if(p.hasPermission(tn.gui.1)){
		Utils.createItem(inv, 4, 1, 1, "&cTest item", "&bThe Nebula");
		//}
		
		
		toReturn.setContents(inv.getContents());
		return toReturn;
		
	}
	
	public static void clicked(Player p, int slot, ItemStack clicked, Inventory inv) {
		if (clicked.getItemMeta().getDisplayName().equalsIgnoreCase(Utils.chat("&cTest item"))) {
			p.setDisplayName(Utils.chat("&d&lThe Nebula &8| &5You have Sucessfully made a GUI"));
		}
	}
	
	
	
}
