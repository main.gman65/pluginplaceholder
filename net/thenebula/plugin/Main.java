package net.thenebula.plugin;

import java.io.IOException;

import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.plugin.java.JavaPlugin;

import net.thenebula.plugin.commands.CurrencyCommand;
import net.thenebula.plugin.commands.FlightCommand;
import net.thenebula.plugin.commands.HelloCommand;
import net.thenebula.plugin.listeners.InventoryClickListener;
import net.thenebula.plugin.listeners.JoinListener;
import net.thenebula.plugin.listeners.PlayerDeathListener;
import net.thenebula.plugin.managers.CurrencyManager;
import net.thenebula.plugin.ui.TestUI;
import net.thenebula.plugin.utils.Utils;

public class Main extends JavaPlugin {

	@Override
	public void onEnable() {
		startup();
		registerManagers();
		CurrencyManager currencyManager = new CurrencyManager(this);
		try {
			currencyManager.loadCurrencyFile();
		} catch (ClassNotFoundException | IOException e) {
			e.printStackTrace();
		}
		registerListeners();
		registerCommands();
		registerUIS();
		saveDefaultConfig();
	}
	
	@Override
	public void onDisable() {
		shutdown();
		CurrencyManager currencyManager = new CurrencyManager(this);
		try {
			currencyManager.saveCurrencyFile();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void shutdown() {
		ConsoleCommandSender console = getServer().getConsoleSender();
		console.sendMessage(Utils.chat("&5======================================================="));
		console.sendMessage(Utils.chat(" "));
		console.sendMessage(Utils.chat(" "));
		console.sendMessage(Utils.chat("&6,    .     |         |             ,---.               "));
		console.sendMessage(Utils.chat("&6|\\  |,---.|---..   .|    ,---.    |    ,---.,---.,---."));
		console.sendMessage(Utils.chat("&6| \\ ||---'|   ||   ||    ,---|    |    |   ||    |---'"));
		console.sendMessage(Utils.chat("&6`   `'`---'`---'`---'`---'`---^    `---'`---'`    `---'"));
		console.sendMessage(Utils.chat(" "));
		console.sendMessage(Utils.chat(" "));
		console.sendMessage(Utils.chat("&6Nebula Core, now DISABLED"));
		console.sendMessage(Utils.chat(" "));
		console.sendMessage(Utils.chat(" "));
		console.sendMessage(Utils.chat("&5======================================================="));	
	}
	
	public void startup() {
		ConsoleCommandSender console = getServer().getConsoleSender();
		console.sendMessage(Utils.chat("&5======================================================="));
		console.sendMessage(Utils.chat(" "));
		console.sendMessage(Utils.chat(" "));
		console.sendMessage(Utils.chat("&6,    .     |         |             ,---.               "));
		console.sendMessage(Utils.chat("&6|\\  |,---.|---..   .|    ,---.    |    ,---.,---.,---."));
		console.sendMessage(Utils.chat("&6| \\ ||---'|   ||   ||    ,---|    |    |   ||    |---'"));
		console.sendMessage(Utils.chat("&6`   `'`---'`---'`---'`---'`---^    `---'`---'`    `---'"));
		console.sendMessage(Utils.chat(" "));
		console.sendMessage(Utils.chat(" "));
		console.sendMessage(Utils.chat("&6Nebula Core, now enabled"));
		console.sendMessage(Utils.chat("&6version: 1.5"));
		console.sendMessage(Utils.chat("&6Developed by: Alexhill2233"));
		console.sendMessage(Utils.chat(" "));
		console.sendMessage(Utils.chat(" "));
		console.sendMessage(Utils.chat("&5======================================================="));
	}
	
	public void registerManagers() {
		new CurrencyManager(this);
	}
	
	public void registerUIS() {
		TestUI.initialize();
	}
	
	public void registerCommands() {
		new FlightCommand(this);
		new HelloCommand(this);
		new CurrencyCommand(this);
	}
	
	public void registerListeners() {
		new JoinListener(this);
		new InventoryClickListener(this);
		new PlayerDeathListener(this);
	}
}
