package net.thenebula.plugin.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.thenebula.plugin.Main;
import net.thenebula.plugin.utils.Utils;

public class HelloCommand implements CommandExecutor {
	
	@SuppressWarnings("unused")
	private Main plugin;
	
	public HelloCommand(Main plugin) {
		this.plugin = plugin;
		
		plugin.getCommand("hello").setExecutor(this);
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!(sender instanceof Player)) {
			sender.sendMessage(Utils.chat("&fOnly players can use this."));
			return true;
		}
		
		Player p = (Player) sender;

		if (p.hasPermission("tn.hello.use")) {
			p.sendMessage(Utils.chat("&d&lThe Nebula &8| &8[&c!&8] &fHello World"));
			return true;
		} else {
			p.sendMessage(Utils.chat("&d&lThe Nebula &8| &8[&c!&8] &fNot enough permissions"));
		} 
		return false;
	}
	
}
